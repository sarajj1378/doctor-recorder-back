import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Date, Document } from 'mongoose';

export type DiseaseDocument = Disease & Document;

@Schema({ timestamps: true })
export class Disease extends Document {
  @Prop({ required: true })
  name: string;

  @Prop({ required: false })
  diagnostic: string;

  @Prop({ required: false })
  doctorAdvice: string;

  @Prop({ required: false })
  tozihat: string;
}

export const DiseaseSchema = SchemaFactory.createForClass(Disease);
