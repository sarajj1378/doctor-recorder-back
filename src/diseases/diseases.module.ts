import {
  DrugSuggestions,
  DrugSuggestionsSchema,
} from './../database/schemas/drugs-suggestion.schema';
import { Disease, DiseaseSchema } from './schema/disease.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';
import { DiseasesService } from './diseases.service';
import { DiseasesController } from './diseases.controller';
import { Drug, DrugSchema } from 'src/drugs/schema/drug.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Disease.name, schema: DiseaseSchema },
      { name: Drug.name, schema: DrugSchema },
      { name: DrugSuggestions.name, schema: DrugSuggestionsSchema },
    ]),
  ],
  controllers: [DiseasesController],
  providers: [DiseasesService],
})
export class DiseasesModule {}
