import { DrugDiseaseConflictsDocument } from './../database/schemas/disease-drug-conflicts.schema';
import { DrugSuggestions } from './../database/schemas/drugs-suggestion.schema';
import { Disease, DiseaseDocument } from './schema/disease.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateDiseaseDto } from './dto/create-disease.dto';
import { UpdateDiseaseDto } from './dto/update-disease.dto';
import { Model } from 'mongoose';
import { Drug, DrugDocument } from 'src/drugs/schema/drug.schema';

@Injectable()
export class DiseasesService {
  constructor(
    @InjectModel(Disease.name) private diseaseModel: Model<DiseaseDocument>,
    @InjectModel(Drug.name) private drugModel: Model<DrugDocument>,
    @InjectModel(DrugSuggestions.name)
    private drugSuggestionsModel: Model<DrugDiseaseConflictsDocument>,
  ) {}

  async create(createDiseaseDto: CreateDiseaseDto) {
    const createdDisease = new this.diseaseModel(createDiseaseDto);
    const savedDisease = await createdDisease.save();
    if (createDiseaseDto.suggestedDrugs?.length > 0)
      for (const suggestedDrug of createDiseaseDto.suggestedDrugs) {
        const drugConflict = new this.drugSuggestionsModel({
          drug: await this.drugModel.findById(suggestedDrug._id).exec(),
          disease: savedDisease,
        });
        await drugConflict.save();
      }
    return savedDisease;
  }

  async findAll() {
    return await this.diseaseModel.find();
  }

  async getSuggestedDrugsById(id: string) {
    return await this.drugSuggestionsModel
      .find({
        disease: await this.diseaseModel.findById(id),
      })
      .populate('drug');
  }

  async findOne(id: string) {
    return this.diseaseModel.findOne({ _id: id });
  }

  update(id: string, updateDiseaseDto: UpdateDiseaseDto) {
    return `This action updates a #${id} disease`;
  }

  remove(id: string) {
    return this.diseaseModel.deleteMany({ _id: id });
  }
}
