import { Drug } from 'src/drugs/schema/drug.schema';
export class CreateDiseaseDto {
  name: string;

  diagnostic: string;

  doctorAdvice: string;

  tozihat: string;

  suggestedDrugs: Drug[];
}
