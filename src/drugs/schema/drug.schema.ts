import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Date, Document } from 'mongoose';

export type DrugDocument = Drug & Document;

@Schema({ timestamps: true })
export class Drug extends Document {
  @Prop({ required: true, unique: true })
  name: string;

  @Prop({ required: true })
  modatMasraf: string;

  @Prop({ required: true })
  consumptionPeriod: string;

  @Prop({ required: true })
  meghdarMsraf: string;

  @Prop({ required: true })
  nahveMasraf: string;

  @Prop({ required: false })
  doctorAdvice: string;
}

export const DrugSchema = SchemaFactory.createForClass(Drug);
