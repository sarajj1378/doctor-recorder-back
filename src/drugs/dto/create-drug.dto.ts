import { Disease } from './../../diseases/schema/disease.schema';
import { Drug } from '../schema/drug.schema';
import {
  IsBoolean,
  IsDate,
  IsDateString,
  IsIn,
  IsNotEmpty,
  IsPhoneNumber,
  Length,
  ValidateIf,
  IsArray,
} from 'class-validator';

export class CreateDrugDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  modatMasraf: string;

  @IsNotEmpty()
  consumptionPeriod: string;

  @IsNotEmpty()
  meghdarMsraf: string;

  @IsNotEmpty()
  nahveMasraf: string;

  doctorAdvice: string;

  conflictDrugs: Drug[];

  conflictDiseases: Disease[];
}
