import { DiseaseSchema } from './../diseases/schema/disease.schema';
import { Disease } from 'src/diseases/schema/disease.schema';
import {
  DrugDiseaseConflicts,
  DrugDiseaseConflictsSchema,
} from './../database/schemas/disease-drug-conflicts.schema';
import { Drug, DrugSchema } from './schema/drug.schema';
import { Module } from '@nestjs/common';
import { DrugsService } from './drugs.service';
import { DrugsController } from './drugs.controller';
import { MongooseModule } from '@nestjs/mongoose';
import {
  DrugsConflicts,
  DrugsConflictsSchema,
} from 'src/database/schemas/drugs-conflicts.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Drug.name, schema: DrugSchema },
      { name: Disease.name, schema: DiseaseSchema },
      { name: DrugsConflicts.name, schema: DrugsConflictsSchema },
      { name: DrugDiseaseConflicts.name, schema: DrugDiseaseConflictsSchema },
    ]),
  ],
  controllers: [DrugsController],
  providers: [DrugsService],
})
export class DrugsModule {}
