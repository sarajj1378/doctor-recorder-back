import {
  DrugDiseaseConflicts,
  DrugDiseaseConflictsDocument,
} from './../database/schemas/disease-drug-conflicts.schema';
import { DrugsConflicts } from './../database/schemas/drugs-conflicts.schema';
import { Drug, DrugDocument } from './schema/drug.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateDrugDto } from './dto/create-drug.dto';
import { UpdateDrugDto } from './dto/update-drug.dto';
import { Model } from 'mongoose';
import { DrugsConflictsDocument } from 'src/database/schemas/drugs-conflicts.schema';
import { Disease, DiseaseDocument } from 'src/diseases/schema/disease.schema';

@Injectable()
export class DrugsService {
  constructor(
    @InjectModel(Drug.name) private drugModel: Model<DrugDocument>,
    @InjectModel(Disease.name) private diseaseModel: Model<DiseaseDocument>,
    @InjectModel(DrugsConflicts.name)
    private drugConflictsModel: Model<DrugsConflictsDocument>,
    @InjectModel(DrugDiseaseConflicts.name)
    private drugDiseaseConflictsModel: Model<DrugDiseaseConflictsDocument>,
  ) {}

  async create(createDrugDto: CreateDrugDto) {
    const createdDrug = new this.drugModel(createDrugDto);
    const savedDrug = await createdDrug.save();
    if (createDrugDto.conflictDrugs?.length > 0)
      for (const conflictDrug of createDrugDto.conflictDrugs) {
        const drugConflict = new this.drugConflictsModel({
          conflictDrug: await this.drugModel.findById(conflictDrug._id).exec(),
          drug: savedDrug,
        });
        await drugConflict.save();
      }
    if (createDrugDto.conflictDiseases?.length > 0)
      for (const conflictDisease of createDrugDto.conflictDiseases) {
        const diseaseConflict = new this.drugConflictsModel({
          disease: await this.diseaseModel.findById(conflictDisease._id).exec(),
          drug: savedDrug,
        });
        await diseaseConflict.save();
      }
    return savedDrug;
  }

  async findAll() {
    return await this.drugModel.find();
  }

  async getDrugConflictsById(id: string) {
    return await this.drugConflictsModel
      .find({
        drug: await this.drugModel.findById(id),
      })
      .populate('conflictDrug');
  }

  async getDiseaseConflictsById(id: string) {
    return await this.drugDiseaseConflictsModel
      .find({
        drug: await this.drugModel.findById(id),
      })
      .populate('disease');
  }

  findOne(id: string) {
    return this.drugModel.findOne({ _id: id });
  }

  update(id: string, updateDrugDto: UpdateDrugDto) {
    return `This action updates a #${id} drug`;
  }

  async remove(id: string) {
    this.drugConflictsModel.deleteMany({
      drugId: await this.drugModel.findById(id),
    });
    return this.drugModel.deleteMany({ _id: id });
  }
}
