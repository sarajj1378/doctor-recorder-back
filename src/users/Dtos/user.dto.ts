import {
  IsDate,
  IsEmail,
  IsIn,
  IsNotEmpty,
  IsPhoneNumber,
  Length,
  IsDateString,
} from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty({ message: 'مقدار نام نمی تواند خالی باشد' })
  firstName: string;
  @IsNotEmpty({ message: 'مقدار نام خانوادگی نمی تواند خالی باشد' })
  lastName: string;
  @IsNotEmpty({ message: 'مقدار کد نظام پزشکی نمی تواند خالی باشد' })
  medicalCode: string;
  @IsNotEmpty({ message: 'مقدار رمز عبور نمی تواند خالی باشد' })
  password: string;
  @IsNotEmpty({ message: 'مقدار کد ملی نمی تواند خالی باشد' })
  @Length(10, 10, { message: 'کد ملی باید 10 رقمی باشد' })
  nationalCode: string;
  @IsNotEmpty({ message: 'مقدار تاریخ تولد نمی تواند خالی باشد' })
  @IsDateString()
  birthday: Date;
  @IsNotEmpty({ message: 'مقدار آدرس نمی تواند خالی باشد' })
  address: string;
  @IsNotEmpty({ message: 'مقدار جنسیت نمی تواند خالی باشد' })
  @IsIn(['male', 'female'], { message: 'فرمت جنسیت صحیح نیست' })
  gender: 'male' | 'female';
  @IsNotEmpty({ message: 'مقدار تخصص نمی تواند خالی باشد' })
  speciality: string;
  @IsNotEmpty({ message: 'مقدار شماره موبایل نمی تواند خالی باشد' })
  @IsPhoneNumber('IR', { message: 'فرمت شماره موبایل وارد شده صحیح نیست' })
  phoneNumber: string;
  @IsEmail({}, { message: 'فرمت ایمیل وارد شده صحیح نیست' })
  email: string;
}
