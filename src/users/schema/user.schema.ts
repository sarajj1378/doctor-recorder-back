import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Date, Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema({ timestamps: true })
export class User extends Document {
  @Prop({ required: true, unique: true })
  medicalCode: string;

  @Prop({ required: true })
  nationalCode: string;

  @Prop({ required: true })
  firstName: string;

  @Prop({ required: true })
  lastName: string;

  @Prop({ type: Date, required: true })
  birthday: Date;

  @Prop({ required: true })
  address: string;

  @Prop({ required: true })
  gender: 'male' | 'female';

  @Prop({ required: true })
  speciality: string;

  @Prop({ required: true })
  phoneNumber: string;

  @Prop({ required: false })
  email: string;

  @Prop({ required: true })
  password: string;

  @Prop({ required: true, default: false })
  isAdmin: boolean;
}

export const UserSchema = SchemaFactory.createForClass(User);
