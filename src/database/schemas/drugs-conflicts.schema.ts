import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Drug } from 'src/drugs/schema/drug.schema';

export type DrugsConflictsDocument = DrugsConflicts & Document;

@Schema({ timestamps: true })
export class DrugsConflicts extends Document {
  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: Drug.name,
    required: true,
  })
  drug: Drug;

  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: Drug.name,
    required: true,
  })
  conflictDrug: Drug;
}

export const DrugsConflictsSchema =
  SchemaFactory.createForClass(DrugsConflicts);
