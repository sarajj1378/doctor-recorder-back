import { Disease } from './../../diseases/schema/disease.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Drug } from 'src/drugs/schema/drug.schema';
import { Visit } from 'src/visits/schema/visit.schema';

export type VisitDiseasesDocument = VisitDiseases & Document;

@Schema({ timestamps: true })
export class VisitDiseases extends Document {
  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: Drug.name,
    required: true,
  })
  drug: Drug;

  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: Visit.name,
    required: true,
  })
  visit: Visit;
}

export const VisitDiseasesSchema = SchemaFactory.createForClass(VisitDiseases);
