import { Disease } from './../../diseases/schema/disease.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Drug } from 'src/drugs/schema/drug.schema';
import { Visit } from 'src/visits/schema/visit.schema';

export type VisitDrugsDocument = VisitDrugs & Document;

@Schema({ timestamps: true })
export class VisitDrugs extends Document {
  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: Visit.name,
    required: true,
  })
  visit: Visit;

  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: Disease.name,
    required: true,
  })
  diseaseId: Disease;
}

export const VisitDrugsSchema = SchemaFactory.createForClass(VisitDrugs);
