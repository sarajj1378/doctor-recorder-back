import { Disease } from './../../diseases/schema/disease.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Drug } from 'src/drugs/schema/drug.schema';

export type DrugDiseaseConflictsDocument = DrugDiseaseConflicts & Document;

@Schema({ timestamps: true })
export class DrugDiseaseConflicts extends Document {
  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: Drug.name,
    required: true,
  })
  drug: Drug;

  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: Disease.name,
    required: true,
  })
  disease: Disease;
}

export const DrugDiseaseConflictsSchema =
  SchemaFactory.createForClass(DrugDiseaseConflicts);
