import { User } from './../../users/schema/user.schema';
import { Disease } from 'src/diseases/schema/disease.schema';
import { Drug } from 'src/drugs/schema/drug.schema';
import { Patient } from 'src/patients/schema/patient.schema';

export class CreateVisitDto {
  patient: Patient;

  doctor: User;

  details: string;

  date: Date;

  diseases: Disease[];

  drugs: Drug[];
}
