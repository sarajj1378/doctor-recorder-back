import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Date, Document, Schema as MongooseSchema } from 'mongoose';
import { Disease } from 'src/diseases/schema/disease.schema';
import { Drug } from 'src/drugs/schema/drug.schema';
import { Patient } from 'src/patients/schema/patient.schema';
import { User } from 'src/users/schema/user.schema';

export type VisitDocument = Visit & Document;

@Schema({ timestamps: true })
export class Visit extends Document {
  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: Patient.name,
    required: true,
  })
  patient: Patient;

  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: User.name,
    required: true,
  })
  doctor: User;

  @Prop({ required: false })
  details: string;

  @Prop({ type: Date, required: true })
  date: Date;

  @Prop({
    type: [{ type: MongooseSchema.Types.ObjectId, ref: Disease.name }],
    required: false,
  })
  diseases: Disease[];

  @Prop({
    type: [{ type: MongooseSchema.Types.ObjectId, ref: Drug.name }],
    required: false,
  })
  drugs: Drug[];
}

export const VisitSchema = SchemaFactory.createForClass(Visit);
