import { InjectModel } from '@nestjs/mongoose';
import { Injectable } from '@nestjs/common';
import { CreateVisitDto } from './dto/create-visit.dto';
import { UpdateVisitDto } from './dto/update-visit.dto';
import { Visit, VisitDocument } from './schema/visit.schema';
import { Model } from 'mongoose';

@Injectable()
export class VisitsService {
  constructor(
    @InjectModel(Visit.name) private visitModel: Model<VisitDocument>,
  ) {}

  create(createVisitDto: CreateVisitDto) {
    const createdVisit = new this.visitModel(createVisitDto);
    return createdVisit.save();
  }

  findAll() {
    return this.visitModel
      .find()
      .populate('patient')
      .populate('doctor')
      .populate('diseases')
      .populate('drugs');
  }

  findOne(id: string) {
    return this.visitModel
      .findById(id)
      .populate('patient')
      .populate('doctor')
      .populate('diseases')
      .populate('drugs');
  }

  update(id: string, updateVisitDto: UpdateVisitDto) {
    return `This action updates a #${id} visit`;
  }

  remove(id: string) {
    return this.visitModel.findByIdAndDelete(id);
  }
}
