import { CreatePatientDto } from './dto/create-patient.dto';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { Patient, PatientDocument } from './schema/patient.schema';
import { UpdatePatientDto } from './dto/update-patient.dto';

@Injectable()
export class PatientsService {
  constructor(
    @InjectModel(Patient.name) private patientModel: Model<PatientDocument>,
  ) {}

  async getAll(): Promise<PatientDocument[]> {
    return await this.patientModel.find();
  }

  async create(createPatientDto: CreatePatientDto): Promise<PatientDocument> {
    if (
      await this.patientIsExist({ nationalCode: createPatientDto.nationalCode })
    )
      throw new HttpException(
        'بیمار در سامانه موجود می باشد',
        HttpStatus.CONFLICT,
      );
    const createdPatient = new this.patientModel(createPatientDto);
    return createdPatient.save();
  }

  async patientIsExist(filter: FilterQuery<PatientDocument>): Promise<boolean> {
    return this.patientModel.exists(filter);
  }

  async delete(id: string) {
    return this.patientModel.deleteMany({ _id: id });
  }

  async findOne(id: string): Promise<PatientDocument> {
    return this.patientModel.findOne({ _id: id });
  }

  async update(id: string, updatePatientDto: UpdatePatientDto) {
    // const patient = await this.patientModel.findOne({ _id: id });
    // patient.update({$set:updatePatientDto})
  }
}
