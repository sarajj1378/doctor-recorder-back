import {
  IsBoolean,
  IsDate,
  IsDateString,
  IsIn,
  IsNotEmpty,
  IsPhoneNumber,
  Length,
  ValidateIf,
} from 'class-validator';

export class CreatePatientDto {
  @IsNotEmpty({ message: 'مقدار نام نمی تواند خالی باشد' })
  firstName: string;

  @IsNotEmpty({ message: 'مقدار نام خانوادگی نمی تواند خالی باشد' })
  lastName: string;

  @IsNotEmpty({ message: 'مقدار کد ملی نمی تواند خالی باشد' })
  @Length(10, 10, { message: 'کد ملی باید 10 رقمی باشد' })
  nationalCode: string;

  @IsNotEmpty({ message: 'مقدار تاریخ تولد نمی تواند خالی باشد' })
  @IsDateString({}, { message: 'فرمت تاریخ تولد صحیح نیست' })
  birthday: Date;

  @IsNotEmpty({ message: 'مقدار آدرس نمی تواند خالی باشد' })
  address: string;

  @IsNotEmpty({ message: 'مقدار جنسیت نمی تواند خالی باشد' })
  @IsIn(['male', 'female'], { message: 'فرمت جنسیت صحیح نیست' })
  gender: 'male' | 'female';

  // @IsNotEmpty({ message: 'مقدار شماره موبایل نمی تواند خالی باشد' })
  // @IsPhoneNumber('', { message: 'فرمت شماره موبایل وارد شده صحیح نیست' })
  // phoneNumber: string;

  // @IsNotEmpty({ message: 'مقدار شماره موبایل نمی تواند خالی باشد' })
  // // @IsPhoneNumber('IR', { message: 'فرمت شماره موبایل وارد شده صحیح نیست' })
  // phoneNumber: string;


  // @ValidateIf((o) => o.gender === 'female')
  // @IsBoolean({ message: 'فرمت شماره موبایل وارد شده صحیح نیست' })
  // @IsNotEmpty({ message: 'وضعیت حاملگی نمی تواند خالی باشد' })
  pregnancyStatus: boolean | undefined;

  historyDetails: string;
}
