import { UpdatePatientDto } from './dto/update-patient.dto';
import { CreatePatientDto } from './dto/create-patient.dto';
import { PatientsService } from './patients.service';
import {
  Controller,
  Delete,
  Get,
  Post,
  Query,
  UseGuards,
  Param,
  Patch,
} from '@nestjs/common';
import { Patient, PatientDocument } from './schema/patient.schema';
import { Body } from '@nestjs/common';
import { FilterQuery } from 'mongoose';
import { AuthGuard } from 'src/auth/auth.guard';

@Controller('patients')
// @UseGuards(AuthGuard)
export class PatientsController {
  constructor(private patientsService: PatientsService) {}

  @Get()
  index(): Promise<Patient[]> {
    return this.patientsService.getAll();
  }

  @Post()
  create(@Body() createPatientDto: CreatePatientDto): Promise<Patient> {
    return this.patientsService.create(createPatientDto);
  }

  @Get('exists')
  patientIsExists(
    @Query() filter: FilterQuery<PatientDocument>,
  ): Promise<boolean> {
    return this.patientsService.patientIsExist(filter);
  }

  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.patientsService.delete(id);
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<Patient> {
    return this.patientsService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePatientDto: UpdatePatientDto) {
    return this.patientsService.update(id, updatePatientDto);
  }
}
