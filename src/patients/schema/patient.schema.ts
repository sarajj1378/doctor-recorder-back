import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Date, Document } from 'mongoose';

export type PatientDocument = Patient & Document;

@Schema({ timestamps: true })
export class Patient extends Document {
  @Prop({ required: true, unique: true })
  nationalCode: string;

  @Prop({ required: true })
  firstName: string;

  @Prop({ required: true })
  lastName: string;

  @Prop({ type: Date, required: true })
  birthday: Date;

  @Prop({ required: true })
  address: string;

  @Prop({ required: true })
  gender: 'male' | 'female';

  @Prop({ required: true })
  phoneNumber: string;

  @Prop({ required: false, default: false })
  pregnancyStatus: boolean;

  @Prop({ required: false, default: '' })
  historyDetails: string;
}

export const PatientSchema = SchemaFactory.createForClass(Patient);
